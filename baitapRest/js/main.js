const $ = document.querySelector.bind(document);
const averageScore = (...scores) => {
    let diem = 0
    let count = 0;
    for (let score of scores) {
        diem += score;
        count++
    }
    return diem / count;
};
const khoi1 = () => {
    let toan = $('#inpToan').value*1;
    let ly = $('#inpLy').value*1;
    let hoa = $('#inpHoa').value*1;
    $('#tbKhoi1').innerHTML = `${averageScore(toan, ly, hoa).toFixed(2)}`
}

const khoi2 = () => {
    let toan = $('#inpVan').value*1;
    let ly = $('#inpSu').value*1;
    let hoa = $('#inpDia').value*1;
    let eng = $('#inpEnglish').value*1;
    $('#tbKhoi2').innerHTML = `${averageScore(toan, ly, hoa, eng).toFixed(2)}`
}