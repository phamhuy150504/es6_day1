const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);
const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron","fuschia", "cinnabar"];
renderBtn = () => {
    const divBtn = $('#colorContainer')
    colorList.forEach((item, index) => {
        index == 0 ? divBtn.innerHTML += `<button class='color-button ${item} active'></button>` : divBtn.innerHTML += `<button class='color-button ${item}'></button>`
    })
}, renderBtn();
const arr = $$('.color-button')
arr.forEach(item => {
    item.addEventListener('click', function () {
        arr.forEach(btn => btn.classList.remove('active'))
        this.classList.add('active')
    })
})
const clickChangeColor = () => {
    const house = document.querySelector('#house')
    arr.forEach(btn => {
        btn.onclick = () => {
            let color = btn.className;
            let newColor = color.replace('color-button', '')
            house.className = `house ${newColor}`
        };
    });
}
clickChangeColor();